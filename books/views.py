from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic

from . models import Book
from .forms import BookForm


class BookListView(generic.ListView):
    model = Book


class BookCreateView(generic.CreateView):
    model = Book
    form_class = BookForm
    success_url = reverse_lazy('books:book_list')


class BookUpdateView(generic.UpdateView):
    model = Book
    form_class = BookForm
    success_url = reverse_lazy('books:book_list')


class BookDeleteView(generic.DeleteView):
    model = Book
    success_url = reverse_lazy('books:book_list')


class BookView(generic.DetailView):
    model = Book
