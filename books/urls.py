from django.urls import path

from . import views

app_name = 'books'

urlpatterns = [
    path('', view=views.BookListView.as_view(), name='book_list'),
    path('new', view=views.BookCreateView.as_view(), name='book_new'),
    path('edit/<int:pk>', view=views.BookUpdateView.as_view(), name='book_edit'),
    path('delete/<int:pk>', view=views.BookDeleteView.as_view(), name='book_delete'),
    path('view/<int:pk>', view=views.BookView.as_view(), name='book_view')
]
