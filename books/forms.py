from django import forms

from .models import Book


class BookForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs = {
            'class': 'form-control'
        }
        self.fields['author'].widget.attrs = {
            'class': 'form-control'
        }

    class Meta:
        model = Book
        fields = ('name', 'author', 'read')
